﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class Interpolation_Grid_PositionalData : MonoBehaviour
{
    #region Camera Data
    // ========================================= //
    // Camera Data

    public Camera cameraData;

    private float cameraX;
    private float cameraY;
    private float cameraZ;

    private Vector2 playerPos;

    private int playerCameraX;
    private int playerCameraY;
    private int playerCameraZ;

    #endregion

    #region Grid Data
    // ========================================= //
    // Grid Data    

    private GameObject interpolationGrid;
    private Vector3 gridPointLocation;
    private int gridIdx;

    public Material glassMat;

    // user defined value for the grid resolution
    public int interpolationDistance;

    // floor dimensions

    public GameObject floor;
    private Vector3 floorDimensions;
    private Vector3 floorCenterPoint;

    private float floorCenterX;
    private float floorCenterY;

    private float floorWidth;
    private float floorLength;

    // grid dimensions
    private float gridWidth;
    private float gridLength;
    #endregion

    #region Option 1 for interpolation
    // ---------------------------------------- //
    // OPTION 1
    //  coordinates for each point in the grid

    private double coordinateX;
    private double coordinateY;
    private string coordinateXString;
    private string coordinateYString;

    #endregion

    #region Option 2 for interpolation
    // ---------------------------------------- //
    // OPTION 2
    // ODEON coordinate points list

    private List<string> lines;
    public float gridSphereScale;
    public int numberOfPoints;
    // store the positional data of each grid point
    private string[] ReceiversPos;
    private List<string> ReceiversPosList;

    private float ReceiverPosX;
    private List<float> ReceiverPosXList = new List<float>();
    private float ReceiverPosY;
    private List<float> ReceiverPosYList = new List<float>();

    // ---------------------------------------- //
    // grid matching uer position

    private Vector2[] interpolationSquare;

    private float[] distanceInterpolationArray;
    public List<GameObject> gridPointSelection = new List<GameObject>();
    private float squareDiagonal;


    public List<GameObject> SphereGrid = new List<GameObject>();
    private float[] distanceToSphereArray;
    private List<float> distanceToSphereList = new List<float>();

    // OPTION A variables
    private List<float> normalizedGainList = new List<float>();
    public List<float> weightedSelectedGainList = new List<float>();

    // OPTION B variables
    private float[] NormalizedFullGainArray;
    private float[] distanceToFullGridArray;
    public float[] weightedFullGainArray ;


    public int distanceIDX;

    #endregion



    // ========================================================================================== //

    void Start()
    {
        interpolationGrid = GameObject.Find("Interpolation Grid");
        squareDiagonal = Mathf.Sqrt(Mathf.Pow(interpolationDistance, 2) + Mathf.Pow(interpolationDistance, 2));

        distanceInterpolationArray = new float[4];
        distanceToFullGridArray = new float[56];

        // -------------------------------------------------------------------------------------- //

        #region IMPORTING GRID FROM ODEON
        // this section allows imprting text files from ODEON and tries to find the position of the receivers used in ODEON
        // these receivers correspond to the grid points in the grid generated in unity to interpolate according to the user's position


        // --------------------------------------------------------------------------------------- //
        // 1. Calculate the size of the grid by fetching the dimensions of the floor surface corresponding to the grid
        // floor's width will correspond to the grid's width (x axis)
        // floor's legth will correspond to the grid's length (y axis, WHICH IS Z IN UNITY)

        floorCenterPoint = floor.gameObject.transform.position;
        floorCenterX = floorCenterPoint.x;
        floorCenterY = floorCenterPoint.z;

        floorDimensions = floor.gameObject.GetComponent<Collider>().bounds.size;
        floorWidth = floorDimensions.x;
        floorLength = floorDimensions.z;

        gridWidth = floorWidth - 1;
        gridLength = floorLength - 1;

        Vector2 floorReferencePoint = new Vector2((floorCenterX - floorWidth / 2), (floorCenterY - floorLength / 2));

        // --------------------------------------------------------------------------------------- //
        // 2. Fetch the lines that contaont the Receiver Position
        string path = "Assets/Resources/ODEON_Data/Test_Output.txt";
        string line;

        lines = new List<string>();

        // Read the file and display it line by line.
        StreamReader file = new StreamReader(path);
        while ((line = file.ReadLine()) != null)
        {

            lines.Add(line);
        }

        file.Close();

        // --------------------------------------------------------------------------------------- //

        gridIdx = 1;
        // run throught the list and fetch the positional data of each gridPoint
        foreach (string item in lines)
        {
            // in the text file from ODEON, the Receivers' Position line starts with "Receiver Position" 
            // when that line is found, split the elements into a string array and then convert to a list. 

            if (item.Contains("ReceiverPosition"))
            {
                ReceiversPos = item.Split(null);

                ReceiversPosList = ReceiversPos.ToList();
                ReceiversPosList.Remove("ReceiverPosition"); // remove "Receiver Position" so that the list only contains coordinates

                // parse the coordinates from string values to float 
                ReceiverPosX = float.Parse(ReceiversPos[1]);
                ReceiverPosY = float.Parse(ReceiversPos[3]);

                // create a list of all X and Y coordinates into 2 different lists
                ReceiverPosXList.Add(ReceiverPosX);
                ReceiverPosYList.Add(ReceiverPosY);

                #region GRID VISUALIZATION FOR DEBBUGING (Can be turn off when finished)
                // create a sphere in the position of the grip location, to visualize the distribution of points in the simulated area

                gridPointLocation = new Vector3(ReceiverPosX, 1, ReceiverPosY);             // vector for the position of the sphere
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);       // sphere instanciation
                sphere.transform.position = gridPointLocation;                              // addressing the gridPoint location to the sphere
                sphere.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);                // reducind the radius of the sphere

                sphere.gameObject.AddComponent<AkGameObj>();
                sphere.gameObject.AddComponent<AkAmbient>();

                if (gridIdx < 10)
                {
                    sphere.name = "GridPoint" + "0" + gridIdx;

                }
                else
                {
                    sphere.name = "GridPoint" + gridIdx;

                }
                sphere.transform.parent = interpolationGrid.transform;
                sphere.transform.localScale = new Vector3(gridSphereScale, gridSphereScale, gridSphereScale);

                SphereGrid.Add(sphere);

                gridIdx++;

                #endregion
            }
        }


        #endregion

        distanceToSphereArray = new float[ReceiverPosXList.Count];


        // second approach, to use with Poly~ running an instance for each grid point
        NormalizedFullGainArray = new float[numberOfPoints];
        weightedFullGainArray = new float[56];
    }



    // ========================================================================================== //

    void Update()
    {

        #region CALCULATE INTERPOLATION POINTS AND NORMALISE THE EUCLIDEAN DIST TO 0-1 RANGE
        // there has to be 4 points of possible interpolation arround the player position 

        // if player is inside the building, the possible coordinates are calculated as 
        // for positive X vals: (playerCameraX; playerCameraX+1)
        // for positive Y vals: (playerCameraZ; playerCameraZ+1)

        // for negative X vals: ( playerCameraX-1; playerCameraX)
        // for negative X vals: ( playerCameraZ-1; playerCameraZ)


        // tracking the positional data of the camera and rounding it to 2 decimal places
        cameraX = Mathf.Round(cameraData.gameObject.transform.position.x * 100f) / 100f;
        cameraY = Mathf.Round(cameraData.gameObject.transform.position.y * 100f) / 100f;
        cameraZ = Mathf.Round(cameraData.gameObject.transform.position.z * 100f) / 100f;

        playerPos = new Vector2(cameraX, cameraZ);

        playerCameraX = (int)cameraData.gameObject.transform.position.x;
        playerCameraY = (int)cameraData.gameObject.transform.position.y;
        playerCameraZ = (int)cameraData.gameObject.transform.position.z;


        // ============================================================================================================= //
        // interpolation is dependent on the position of the user in the cartesian axis 

        if (playerCameraX >= 0 && playerCameraZ >= 0)
        {
            interpolationSquare = new Vector2[]
              {
                  new Vector2(playerCameraX, playerCameraZ),
                  new Vector2(playerCameraX, (playerCameraZ + 1)),
                  new Vector2((playerCameraX + 1), playerCameraZ ),
                  new Vector2((playerCameraX + 1), (playerCameraZ + 1))
              };

            int vectorIdx = 0;
            foreach (Vector2 vect in interpolationSquare)
            {
                float vectorDist = Vector2.Distance(vect, playerPos);
                distanceInterpolationArray[vectorIdx] = vectorDist;
                vectorIdx++;
            }

            // run through all gridPoints and check who matches the coordinate points of the interpolation grid
            foreach (GameObject gridPoint in SphereGrid)
            {
                foreach (Vector2 vect in interpolationSquare)
                {
                    if (vect.x == gridPoint.transform.position.x && vect.y == gridPoint.transform.position.z)
                    {
                        gridPoint.GetComponent<MeshRenderer>().material.color = Color.blue;
                        if (!gridPointSelection.Contains(gridPoint))
                        {
                            gridPointSelection.Add(gridPoint);
                        }

                        break;
                    }
                    else
                    {
                        // for the ones that don't match, address color yellow and remove from the selected gridPoints list
                        gridPoint.GetComponent<MeshRenderer>().material.color = Color.yellow;
                        if (gridPointSelection.Contains(gridPoint) && gridPoint.GetComponent<MeshRenderer>().material.color == Color.yellow)
                        {
                            gridPointSelection.Remove(gridPoint);
                        }
                    }
                }
            }


        }


        #region FOR OTHER CARTESIAN QUARTERS
        else if (playerCameraX < 0 && playerCameraZ > 0)
        {
            interpolationSquare = new Vector2[]
              {
                  new Vector2((playerCameraX - 1), playerCameraZ),
                  new Vector2((playerCameraX - 1), (playerCameraZ + 1)),
                  new Vector2(playerCameraX, playerCameraZ),
                  new Vector2(playerCameraX, (playerCameraZ + 1))
              };
        }
        else if (playerCameraX > 0 && playerCameraZ < 0)
        {
            interpolationSquare = new Vector2[]
              {
                  new Vector2(playerCameraX, (playerCameraZ - 1)),
                  new Vector2(playerCameraX, playerCameraZ),
                  new Vector2((playerCameraX + 1), (playerCameraZ - 1)),
                  new Vector2((playerCameraX + 1), playerCameraZ)
              };
        }
        else if (playerCameraX < 0 && playerCameraZ < 0)
        {
            interpolationSquare = new Vector2[]
              {
                  new Vector2((playerCameraX - 1), (playerCameraZ - 1)),
                  new Vector2((playerCameraX - 1), playerCameraZ),
                  new Vector2(playerCameraX, (playerCameraZ - 1)),
                  new Vector2(playerCameraX, playerCameraZ)
              };
        }
        #endregion




        #endregion

        // ============================================================================================================= //


        #region OPTION A: to use with Poly~ running an instance for each selected gridPoint + the possible extended area of grid points = Total of 16 points (max)

        // ********************************  NOT FINISHED!!!!! ********************************************************* //

        int gridIdx = 0;
        foreach (GameObject gridEle in gridPointSelection)
        {
            // calculate the distance between each sphere and the camera
            float distanceToSphere = Vector3.Distance(gridEle.transform.position, cameraData.transform.position);

            distanceToSphereArray[gridIdx] = distanceToSphere;

            if (!distanceToSphereList.Contains(distanceToSphere))
            {
                distanceToSphereList.Insert(gridIdx, distanceToSphere);

                // normalize for the range of 0 - 1.41 (diagonal of the square)
                normalizedGainList.Insert(gridIdx, (squareDiagonal - distanceToSphere) / squareDiagonal);

            }

            if (distanceToSphereList.Count > gridPointSelection.Count)
            {
                distanceToSphereList.RemoveRange(gridPointSelection.Count, distanceToSphereList.Count- gridPointSelection.Count);
                normalizedGainList.RemoveRange(gridPointSelection.Count, normalizedGainList.Count - gridPointSelection.Count);
            }

            gridIdx++;
        }


        // ============================================================================================================= //
        // when the selected grid points are less then the array size for the gains, the excess needs to be filled wit "0" gain
        // check how many more indeces are there
        //distanceIDX = normalizedGainList.Length - gridPointSelection.Count;


        float normalizedGainSum = normalizedGainList.Sum();

        // after normalizing all the elements that are active, sum them and normalize the distance gain
        // finally update the value in the distanceGainList

        int WeightIdx = 0;
        foreach (float normVal in normalizedGainList)
        {
            if (!weightedSelectedGainList.Contains(normVal))
            {
                // weight each grid point gain according to the distance to the user and round up to 2 DP
                float gainElement = Mathf.Round(normVal / normalizedGainSum * 100f) / 100f;

                weightedSelectedGainList.Insert(WeightIdx, gainElement);
            }
            if (weightedSelectedGainList.Count > gridPointSelection.Count)
            {
                weightedSelectedGainList.RemoveRange(gridPointSelection.Count, weightedSelectedGainList.Count - gridPointSelection.Count);
            }


            WeightIdx++;
        }






        // list with the gain for all the objects
        int WeightIdx2 = 0;
        foreach (GameObject gridP in SphereGrid)
        {
            if (gridPointSelection.Contains(gridP))
            {
                int elementIdx = gridPointSelection.IndexOf(gridP);

                float weightedVal = weightedSelectedGainList.ElementAt(elementIdx);
                weightedFullGainArray[WeightIdx2] = weightedVal;
            }
            else
            {
                weightedFullGainArray[WeightIdx2] = 0f;
            }

            WeightIdx2++;
        }











        #endregion








        #region OPTION B: to use with [Poly~] running an instance for each grid point


        //int fullGridIdx = 0;
        //foreach (GameObject gridEle in SphereGrid)
        //{
        //    // calculate the distance between each sphere and the camera
        //    float distanceToSphere = Vector3.Distance(gridEle.transform.position, cameraData.transform.position);
        //    distanceToFullGridArray[fullGridIdx] = distanceToSphere;

        //    // normalize for the range of 0 - 1.41 (diagonal of the square)
        //    NormalizedFullGainArray[fullGridIdx] = Mathf.Abs((squareDiagonal - distanceToSphere) / squareDiagonal);

        //    fullGridIdx++;
        //}


        //for (int i = 0; i < gridPointSelection.Count; i++)
        //{
        //    // weight each grid point gain according to the distance to the user and round up to 2 DP
        //    float elementGain = Mathf.Round(NormalizedFullGainArray[i] / normalizedGainSum * 100) / 100;

        //    weightedFullGainArray[i] = elementGain;
        //}

        //for (int k = gridPointSelection.Count; k < NormalizedFullGainArray.Length; k++)
        //{
        //    weightedFullGainArray[k] = 0f;
        //}



        //for (int k = 0; k < NormalizedFullGainArray.Length; k++)
        //{
        //    // if bigger than 0, then it belongs to the interpolation square
        //    if (NormalizedFullGainArray[k] >= 0.0001f)
        //    {
        //        // weight each grid point gain according to the distance to the user and round up to 2 DP
        //        float elementGain = Mathf.Round(NormalizedFullGainArray[k] / normalizedGainSum * 100) / 100 ;

        //        //distanceGainList[j] = Mathf.Lerp(distanceGainList[j], gainElement, (fadeTimer / fadeTime));
        //        weightedFullGainArray[k] = elementGain;

        //    }
        //    // if normVal lower than 0, it does NOT belong to the interpolation square
        //    else if (NormalizedFullGainArray[k] < -0.001)
        //    {
        //        weightedFullGainArray[k] = 0f;
        //    }

        //}






        #endregion







    }
}