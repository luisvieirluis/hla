﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class Interpolation_Grid_old : MonoBehaviour
{
    #region Camera Data
    // ========================================= //
    // Camera Data

    public Camera cameraData;

    private float cameraX;
    private float cameraY;
    private float cameraZ;

    private Vector2 playerPos;

    private int playerCameraX;
    private int playerCameraY;
    private int playerCameraZ;

    #endregion

    #region Grid Data
    // ========================================= //
    // Grid Data    

    private GameObject interpolationGrid;
    private Vector3 gridPointLocation;
    private int gridIdx;

    public Material glassMat;

    // user defined value for the grid resolution
    public int interpolationDistance;

    // floor dimensions

    public GameObject floor;
    private Vector3 floorDimensions;
    private Vector3 floorCenterPoint;

    private float floorCenterX;
    private float floorCenterY;

    private float floorWidth;
    private float floorLength;

    // grid dimensions
    private float gridWidth;
    private float gridLength;
    #endregion

    #region Option 1 for interpolation
    // ---------------------------------------- //
    // OPTION 1
    //  coordinates for each point in the grid

    private double coordinateX;
    private double coordinateY;
    private string coordinateXString;
    private string coordinateYString;

    #endregion

    #region Option 2 for interpolation
    // ---------------------------------------- //
    // OPTION 2
    // ODEON coordinate points list

    private List<string> lines;
    public float gridSphereScale;

    // store the positional data of each grid point
    private string[] ReceiversPos;
    private List<string> ReceiversPosList;

    private float ReceiverPosX;
    private List<float> ReceiverPosXList = new List<float>();
    private float ReceiverPosY;
    private List<float> ReceiverPosYList = new List<float>();

    // ---------------------------------------- //
    // grid matching uer position

    private Vector2[] interpolationSquare;

    private float[] distanceInterpolationArray;
    public List<GameObject> gridPointSelection = new List<GameObject>();


    public List<GameObject> SphereGrid = new List<GameObject>();
    private float[] distanceToSphereArray;
    private float[] normalizedGainList;
    public float[] distanceGainList;
    private float squareDiagonal;


    public int distanceIDX;

    #endregion

    #region Audio Manager list
    // ---------------------------------------- //
    // Audio Manager Trigger

    private List<string> audioID = new List<string>();
    private List<GameObject> selectedGridPoints = new List<GameObject>();
    public float fadeTime;
    private float fadeTimer;

    #endregion

    // ========================================================================================== //

    void Start()
    {
        interpolationGrid = GameObject.Find("Interpolation Grid");
        squareDiagonal = Mathf.Sqrt(Mathf.Pow(interpolationDistance, 2) + Mathf.Pow(interpolationDistance, 2));

        fadeTimer = 0.0f;

        #region GRID AUTOMATIC GENERATION (OPTION 1)
        /*
           // --------------------------------------------------------------------------------------- //
           // 1. Calculate the size of the grid by fetching the dimensions of the floor surface corresponding to the grid
           // floor's width will correspond to the grid's width (x axis)
           // floor's legth will correspond to the grid's length (y axis, WHICH IS Z IN UNITY)

           floorCenterPoint = floor.gameObject.transform.position;
           floorCenterX = floorCenterPoint.x;
           floorCenterY = floorCenterPoint.z;

           floorDimensions = floor.gameObject.GetComponent<Collider>().bounds.size;
           floorWidth = floorDimensions.x;
           floorLength = floorDimensions.z;

           gridWidth = floorWidth - 1;
           gridLength = floorLength - 1;

           Vector2 floorReferencePoint = new Vector2((floorCenterX - floorWidth / 2), (floorCenterY - floorLength / 2));
           //Debug.Log("Floor Reference " + floorReferencePoint);
           // --------------------------------------------------------------------------------------- //
           // 2. Calculate the coordinates for each point in the grid

           // initialize 2D string array to create an array with the coordinates for each point in the grid
           string[,] coordinatePointArray = new string[(int)gridWidth, (int)gridLength];

           gridIdx = 1;
           // for each row value, there has to be all the collum values
           // the loop creates a string value for each of these coordinate points
           for (int rowIdx = 0; rowIdx < gridWidth; rowIdx++)
           {
               double coordinateX = Math.Round((floorReferencePoint.x + (interpolationDistance + rowIdx)) * 100) / 100;
               string coordinateXString = coordinateX.ToString();

               for (int colIdx = 0; colIdx < gridLength; colIdx++)
               {
                   double coordinateY = Math.Round((floorReferencePoint.y + (interpolationDistance + colIdx)) * 100) / 100;
                   string coordinateYString = coordinateY.ToString();

                   coordinatePointArray[rowIdx, colIdx] = String.Format("{0};{1}", coordinateX, coordinateY);


                   #region GRID VISUALIZATION FOR DEBBUGING (Can be turn off when finished)
                   // create a sphere in the position of the grip location, to visualize the distribution of points in the simulated area

                   gridPointLocation = new Vector3((float)coordinateX, 1, (float)coordinateY); // vector for the position of the sphere
                   GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);       // sphere instanciation
                   sphere.transform.position = gridPointLocation;                              // addressing the gridPoint location to the sphere
                   sphere.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);                // reducind the radius of the sphere
                   sphere.name = "GridPoint" + gridIdx;
                   sphere.transform.parent = interpolationGrid.transform;

                   gridIdx++;

                   #endregion

               }
           }


           //gridCoordinateList = new List<string>();

        */
        #endregion

        distanceInterpolationArray = new float[4];

        // -------------------------------------------------------------------------------------- //

        #region IMPORTING GRID FROM ODEON
        // this section allows imprting text files from ODEON and tries to find the position of the receivers used in ODEON
        // these receivers correspond to the grid points in the grid generated in unity to interpolate according to the user's position


        // --------------------------------------------------------------------------------------- //
        // 1. Calculate the size of the grid by fetching the dimensions of the floor surface corresponding to the grid
        // floor's width will correspond to the grid's width (x axis)
        // floor's legth will correspond to the grid's length (y axis, WHICH IS Z IN UNITY)

        floorCenterPoint = floor.gameObject.transform.position;
        floorCenterX = floorCenterPoint.x;
        floorCenterY = floorCenterPoint.z;

        floorDimensions = floor.gameObject.GetComponent<Collider>().bounds.size;
        floorWidth = floorDimensions.x;
        floorLength = floorDimensions.z;

        gridWidth = floorWidth - 1;
        gridLength = floorLength - 1;

        Vector2 floorReferencePoint = new Vector2((floorCenterX - floorWidth / 2), (floorCenterY - floorLength / 2));

        // --------------------------------------------------------------------------------------- //
        // 2. Fetch the lines that contaont the Receiver Position
        string path = "Assets/Resources/ODEON_Data/Test_Output.txt";
        string line;

        lines = new List<string>();

        // Read the file and display it line by line.
        StreamReader file = new StreamReader(path);
        while ((line = file.ReadLine()) != null)
        {

            lines.Add(line);
        }

        file.Close();

        // --------------------------------------------------------------------------------------- //

        gridIdx = 1;
        // run throught the list and fetch the positional data of each gridPoint
        foreach (string item in lines)
        {
            // in the text file from ODEON, the Receivers' Position line starts with "Receiver Position" 
            // when that line is found, split the elements into a string array and then convert to a list. 

            if (item.Contains("ReceiverPosition"))
            {
                ReceiversPos = item.Split(null);

                ReceiversPosList = ReceiversPos.ToList();
                ReceiversPosList.Remove("ReceiverPosition"); // remove "Receiver Position" so that the list only contains coordinates

                // parse the coordinates from string values to float 
                ReceiverPosX = float.Parse(ReceiversPos[1]);
                ReceiverPosY = float.Parse(ReceiversPos[3]);

                // create a list of all X and Y coordinates into 2 different lists
                ReceiverPosXList.Add(ReceiverPosX);
                ReceiverPosYList.Add(ReceiverPosY);

                #region GRID VISUALIZATION FOR DEBBUGING (Can be turn off when finished)
                // create a sphere in the position of the grip location, to visualize the distribution of points in the simulated area

                gridPointLocation = new Vector3(ReceiverPosX, 1, ReceiverPosY);             // vector for the position of the sphere
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);       // sphere instanciation
                sphere.transform.position = gridPointLocation;                              // addressing the gridPoint location to the sphere
                sphere.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);                // reducind the radius of the sphere

                sphere.gameObject.AddComponent<AkGameObj>();
                sphere.gameObject.AddComponent<AkAmbient>();

                if (gridIdx < 10)
                {
                    sphere.name = "GridPoint" + "0" + gridIdx;

                }
                else
                {
                    sphere.name = "GridPoint" + gridIdx;

                }
                sphere.transform.parent = interpolationGrid.transform;
                sphere.transform.localScale = new Vector3(gridSphereScale, gridSphereScale, gridSphereScale);

                SphereGrid.Add(sphere);

                gridIdx++;

                #endregion
            }
        }

        distanceToSphereArray = new float[ReceiverPosXList.Count];
        distanceGainList = new float[4] { 0, 0, 0, 0 };
        normalizedGainList = new float[4] { 0, 0, 0, 0 };


        #endregion
    }





    // ========================================================================================== //

    // Update is called once per frame
    void Update()
    {
        fadeTimer += Time.deltaTime;

        #region CALCULATE INTERPOLATION POINTS AND NORMALISE THE EUCLIDEAN DIST TO 0-1 RANGE
        // there has to be 4 points of possible interpolation arround the player position 

        // if player is inside the building, the possible coordinates are calculated as 
        // for positive X vals: (playerCameraX; playerCameraX+1)
        // for positive Y vals: (playerCameraZ; playerCameraZ+1)

        // for negative X vals: ( playerCameraX-1; playerCameraX)
        // for negative X vals: ( playerCameraZ-1; playerCameraZ)


        // tracking the positional data of the camera and rounding it to 2 decimal places
        cameraX = Mathf.Round(cameraData.gameObject.transform.position.x * 100f) / 100f;
        cameraY = Mathf.Round(cameraData.gameObject.transform.position.y * 100f) / 100f;
        cameraZ = Mathf.Round(cameraData.gameObject.transform.position.z * 100f) / 100f;

        playerPos = new Vector2(cameraX, cameraZ);

        playerCameraX = (int)cameraData.gameObject.transform.position.x;
        playerCameraY = (int)cameraData.gameObject.transform.position.y;
        playerCameraZ = (int)cameraData.gameObject.transform.position.z;


        // ============================================================================================================= //
        // interpolation is dependent on the position of the user in the cartesian axis 

        if (playerCameraX >= 0 && playerCameraZ >= 0)
        {
            interpolationSquare = new Vector2[]
              {
                  new Vector2(playerCameraX, playerCameraZ),
                  new Vector2(playerCameraX, (playerCameraZ + 1)),
                  new Vector2((playerCameraX + 1), playerCameraZ ),
                  new Vector2((playerCameraX + 1), (playerCameraZ + 1))
              };

            int vectorIdx = 0;
            foreach (Vector2 vect in interpolationSquare)
            {
                float vectorDist = Vector2.Distance(vect, playerPos);
                distanceInterpolationArray[vectorIdx] = vectorDist;
                vectorIdx++;
            }

            // run through all gridPoints and check who matches the coordinate points of the interpolation grid
            foreach (GameObject gridPoint in SphereGrid)
            {
                foreach (Vector2 vect in interpolationSquare)
                {
                    if (vect.x == gridPoint.transform.position.x && vect.y == gridPoint.transform.position.z)
                    {
                        gridPoint.GetComponent<MeshRenderer>().material.color = Color.blue;
                        if (!gridPointSelection.Contains(gridPoint))
                        {
                            gridPointSelection.Add(gridPoint);
                        }

                        break;
                    }
                    else
                    {
                        // for the ones that don't match, address color yellow and remove from the selected gridPoints list
                        gridPoint.GetComponent<MeshRenderer>().material.color = Color.yellow;
                        if (gridPointSelection.Contains(gridPoint) && gridPoint.GetComponent<MeshRenderer>().material.color == Color.yellow)
                        {
                            gridPointSelection.Remove(gridPoint);
                        }
                    }
                }
            }
        }


        #region FOR OTHER CARTESIAN QUARTERS
        else if (playerCameraX < 0 && playerCameraZ > 0)
        {
            interpolationSquare = new Vector2[]
              {
                  new Vector2((playerCameraX - 1), playerCameraZ),
                  new Vector2((playerCameraX - 1), (playerCameraZ + 1)),
                  new Vector2(playerCameraX, playerCameraZ),
                  new Vector2(playerCameraX, (playerCameraZ + 1))
              };
        }
        else if (playerCameraX > 0 && playerCameraZ < 0)
        {
            interpolationSquare = new Vector2[]
              {
                  new Vector2(playerCameraX, (playerCameraZ - 1)),
                  new Vector2(playerCameraX, playerCameraZ),
                  new Vector2((playerCameraX + 1), (playerCameraZ - 1)),
                  new Vector2((playerCameraX + 1), playerCameraZ)
              };
        }
        else if (playerCameraX < 0 && playerCameraZ < 0)
        {
            interpolationSquare = new Vector2[]
              {
                  new Vector2((playerCameraX - 1), (playerCameraZ - 1)),
                  new Vector2((playerCameraX - 1), playerCameraZ),
                  new Vector2(playerCameraX, (playerCameraZ - 1)),
                  new Vector2(playerCameraX, playerCameraZ)
              };
        }
        #endregion









        #endregion

        // ============================================================================================================= //

        #region GRID MATCHING
        /*
        // according to where the user is, the nearest points lower than the interpolation distance are 
        // considered the interpolation points

        float distanceToClosestSphere = Mathf.Infinity; // init distance to infinite
        GameObject closestSphere = null;

        int i = 0;

        foreach (GameObject currentSphere in SphereGrid)
        {
            // calculate the distance between each sphere and the camera
            float distanceToSphere = Vector3.Distance(currentSphere.transform.position, cameraData.transform.position);

            distanceToSphereArray[i] = distanceToSphere;
            Array.Sort(distanceToSphereArray);


            // if distance to each point is less than the max distance acceptable, 
            // change the color to green and add gripPoints to AudioID list

            if (distanceToSphere < squareDiagonal)
            {
                distanceToClosestSphere = distanceToSphere;
                closestSphere = currentSphere;

                if (audioID.Count < 4)
                {
                    if (!audioID.Contains(closestSphere.gameObject.GetComponent<MeshRenderer>().name))
                    {
                        //closestSphere.gameObject.GetComponent<MeshRenderer>().material.color = Color.green;

                        audioID.Add(closestSphere.gameObject.name);
                        selectedGridPoints.Add(GameObject.Find(closestSphere.gameObject.name));
                    }
                }
            }
            else if (distanceToSphere > squareDiagonal)
            {
                //currentSphere.gameObject.GetComponent<MeshRenderer>().material = glassMat;
                audioID.Remove(currentSphere.gameObject.GetComponent<MeshRenderer>().name);
                selectedGridPoints.Remove(GameObject.Find(currentSphere.gameObject.GetComponent<MeshRenderer>().name));
            }

            i++;
        }
        */
        #endregion



        #region FETCH THE DISTANCE FOR ACTIVE ELEMENTS AND WEIGHT GAIN FOR EACH

        int gridIdx = 0;
        foreach (GameObject gridEle in gridPointSelection)
        {
            // calculate the distance between each sphere and the camera
            float distanceToSphere = Vector3.Distance(gridEle.transform.position, cameraData.transform.position);
            distanceToSphereArray[gridIdx] = distanceToSphere;

            gridIdx++;
        }


        for (int j = 0; j < gridPointSelection.Count; j++)
        {
            // normalize for the range of 0 - 1.41 (diagonal of the square)
            normalizedGainList[j] = (squareDiagonal - distanceToSphereArray[j]) / squareDiagonal;
        }


        // ============================================================================================================= //
        // when the selected grid points are less then the array size for the gains, the excess needs to be filled wit "0" gain
        // check how many more indeces are there
        //distanceIDX = normalizedGainList.Length - gridPointSelection.Count;

        //// for all the extra indeces, attribute 0 gain
        //if (distanceIDX != 0)
        //{
        //    for (int idx = 0; idx < distanceIDX; idx++)
        //    {
        //        int eleIDx = gridPointSelection.Count + idx;
        //        normalizedGainList[eleIDx] = 0f;
        //    }
        //}





        float normalizedGainSum = normalizedGainList.Sum();



        // after normalizing all the elements that are active, sum them and normalize the distance gain
        // finally update the value in the distanceGainList
        for (int j = 0; j < distanceGainList.Length; j++)
        {
            // weight each grid point gain according to the distance to the user and round up to 2 DP
            float gainElement = Mathf.Round(normalizedGainList[j] / normalizedGainSum * 100f) / 100f;

            //distanceGainList[j] = Mathf.Lerp(distanceGainList[j], gainElement, (fadeTimer / fadeTime));
            distanceGainList[j] = gainElement;
        }

        // all elements that are not active are removed from the list and zerod
        for (int k = gridPointSelection.Count; k < distanceGainList.Length; k++)
        {
            distanceGainList[k] = 0f;
        }


        #endregion




        // ============================================================================================================= //


    }



}