﻿using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour
{


    #region Camera Data
    // ========================================= //
    // Camera Data

    public Camera cameraData;

    private double cameraX;
    private double cameraY;
    private double cameraZ;

    private int playerCameraX;
    private int playerCameraY;
    private int playerCameraZ;

    #endregion

    #region Audio Manager

    public GameObject interpolationGrid;
    public GameObject interpolationObjScript;

    // list storing all the grid point elements
    private List<GameObject> gridList = new List<GameObject>();

    // float arrays with the same size of the grid list, for 
    private float[] distanceToSphere;       // distance between listener and grid point
    private float[] distanceGain;           // gain difference between listener and grid point

    // flag bool to understand if audio for selected grid is playing or not
    public bool[] idxPlayBool;

    #endregion

    #region WWISE LOAD BANK

    uint bankID;

    protected void LoadBank()
    {
        AkSoundEngine.LoadBank("Instrument_Case2", AkSoundEngine.AK_DEFAULT_POOL_ID, out bankID);
    }

    #endregion





    // Start is called before the first frame update
    void Start()
    {
        interpolationGrid = GameObject.Find("Interpolation Grid");
        interpolationObjScript = GameObject.Find("InterpolScript");

        gridList = interpolationObjScript.GetComponent<Interpolation_Grid_PositionalData>().SphereGrid;

        distanceToSphere = new float[56];
        distanceGain = new float[56];

        //idxPlayBool = new bool[56];
        //for (int i = 0; i < distanceGain.Length; i++)
        //{
        //    idxPlayBool[i] = false;

        //}

    }

    // Update is called once per frame
    void Update()
    {

        #region Distance to object and gain calculation

        int idx = 0;

        foreach (GameObject obj in gridList)
        {
            // calculate the distance between each sphere and the camera
            distanceToSphere[idx] = (obj.transform.position - cameraData.transform.position).sqrMagnitude;

            // normalize for the range of 0 - 1.41 (diagonal of the square)
            distanceGain[idx] = (1.41f - distanceToSphere[idx]) / 1.41f;

            // send gain value for all grid points
            AkSoundEngine.SetRTPCValue("interpolation", distanceGain[idx], obj, 50);

  
            idx++;
        }

        #endregion


        #region OPTION A: Play/Stop Sound
        /*
        if (Input.GetKeyUp(KeyCode.Space))
        {
            // init closeSphere to null
            GameObject closestSphere = null;

            int jdx = 0;

            foreach (float dist in distanceToSphere)
            {
                if (dist < 1f && !idxPlayBool[jdx])
                {

                    closestSphere = gridList[jdx];
                    closestSphere.gameObject.GetComponent<MeshRenderer>().material.color = Color.green;

                    // --------------------------------------------------------------------------------------- //
                    // 3. play the audio that just stopped, again.

                    string gridPlayString = "P" + (jdx + 1);

                    AkSoundEngine.PostEvent(gridPlayString, closestSphere);

                    idxPlayBool[jdx] = true;
                }

                else if (dist > 1f && idxPlayBool[jdx])
                {
                    gridList[jdx].gameObject.GetComponent<MeshRenderer>().material.color = Color.white;

                    string gridStopString = "P" + (jdx + 1);

                    uint eventID;
                    eventID = AkSoundEngine.GetIDFromString(gridStopString);
                    AkSoundEngine.ExecuteActionOnEvent(eventID, AkActionOnEventType.AkActionOnEventType_Pause, gridList[jdx], 500, AkCurveInterpolation.AkCurveInterpolation_Sine);


                    idxPlayBool[jdx] = false;
                }
                jdx++;
            }
        }
        */
        #endregion


        #region OPTION B: Play/Stop Sound

        //  using this option, the script simulates the same behaviour as in the PD patch.
        //  this means that all the audio files are played simulatenously everytime the sound source is triggered
        // a calibration fo the gain values is then used to mute and interpolate between grid points

        if (Input.GetKeyUp(KeyCode.L))
        {
           
            int jdx = 0;

            foreach (float dist in distanceGain)
            {
        
                // --------------------------------------------------------------------------------------- //
                // play the audio for all the grid points, taking in account that some are going to be muted in the begining of the update method

                string gridPlayString = "P" + (jdx + 1);
                AkSoundEngine.PostEvent(gridPlayString, gridList[jdx]);

                jdx++;
            }
        }

        // to stop all audio files, press P
        if (Input.GetKeyUp(KeyCode.P))
        {
            int kdx = 0;

            foreach (GameObject ele in gridList)
            {
                string gridStopString = "P" + (kdx + 1);

                uint eventID;
                eventID = AkSoundEngine.GetIDFromString(gridStopString);
                AkSoundEngine.ExecuteActionOnEvent(eventID, AkActionOnEventType.AkActionOnEventType_Stop, ele, 500, AkCurveInterpolation.AkCurveInterpolation_Sine);
                kdx++;
            }
           
        }

        // ********************************************************** //
        //                  CASE SWITCH
        // ********************************************************** //

        // able to switch between as many cases as required. 
        // there are file name formatting requirements. 

        

        #endregion

    }
}
