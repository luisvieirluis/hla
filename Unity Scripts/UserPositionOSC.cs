﻿using System.Collections.Generic;
using UnityEngine;

public class UserPositionOSC : MonoBehaviour
{

    public OSC osc;

    public Camera cameraData;
    public GameObject activeGridPoints;

    public List<GameObject> selectedGridList = new List<GameObject>();
    public float[] weightedGains = new float[4];
    public float[] weightedFullListGains = new float[56];
    public List<string> gridPointVal = new List<string>();
    public int gridPointChange;


    private Vector3 currentPlayerPos;
    private Vector3 previousPlayerPos;

    private float azi;
    private float ele;
    private float alpha;


    #region SWITCH CASE variables

    private int caseIdx = 5;
    #endregion

    private void Start()
    {
        previousPlayerPos = new Vector3(0, 0, 0);
        currentPlayerPos = new Vector3(0, 0, 0);
    }


    void Update()
    {

        #region PLAY CONTROL
        // ============================================================================ //
        // Press Space to play audio

        if (Input.GetKeyUp(KeyCode.Space))
        {
            OscMessage messagePlay = new OscMessage();
            messagePlay.address = "/playAudio";

            foreach (float gainVal in weightedFullListGains)
            {
                messagePlay.values.Add(1);
                osc.Send(messagePlay);
            }
        }

        #endregion


        #region CAMERA POSITIONAL DATA
        // ============================================================ //
        // Camera rotational data (azimuth and elevation)


        azi = cameraData.transform.eulerAngles.y;
        ele = cameraData.transform.eulerAngles.x;

        OscMessage messageHeadAzi = new OscMessage();
        messageHeadAzi.address = "/azi";
        messageHeadAzi.values.Add(azi);
        osc.Send(messageHeadAzi);

        OscMessage messageHeadEle = new OscMessage();
        messageHeadEle.address = "/ele";
        messageHeadEle.values.Add(ele);
        osc.Send(messageHeadEle);


        // angle of ambisonic rotation 

        if (azi > 180f)
        {
            alpha = 360f - azi;
        }
        else if (azi <= 180f)
        {
            alpha = -azi;
        }

        OscMessage messageAlpha = new OscMessage();
        messageAlpha.address = "/alpha";
        messageAlpha.values.Add(alpha);
        osc.Send(messageAlpha);



        // player positional data
        currentPlayerPos.x = cameraData.transform.position.x;
        currentPlayerPos.y = cameraData.transform.position.y;
        currentPlayerPos.z = cameraData.transform.position.z;

        #endregion



        #region NORMALIZED AND WEIGHTED GAIN VALUES
        // ============================================================================ //
        // Normalized and Weighted Gain Values

        weightedFullListGains = activeGridPoints.GetComponent<Interpolation_Grid_PositionalData>().weightedFullGainArray;


        if (previousPlayerPos != currentPlayerPos)
        {
            // OSC message with each gridPoint value and idx
            OscMessage messageWeightedGain = new OscMessage();
            messageWeightedGain.address = "/weightedGain";

            OscMessage messagedIdxGain = new OscMessage();
            messagedIdxGain.address = "/idxGain";

            int k = 1;
            // go throught the complete list of elements in the grid and send OSC message with the gain value associated with each of this points
            foreach (float gainVal in weightedFullListGains)
            {

                messageWeightedGain.values.Add(gainVal);
                osc.Send(messageWeightedGain);

                messagedIdxGain.values.Add(k);
                osc.Send(messagedIdxGain);

        

                k++;
            }

            previousPlayerPos = currentPlayerPos;
        }


        #endregion

        #region SWTICH CASE CONTROL
        // ============================================================================ //
        // Switch Case

        OscMessage messageSwitchCase = new OscMessage();
        messageSwitchCase.address = "/switchCase";

        for (int k = 1; k < caseIdx; k++)
        {
            string keyString = "Keypad" + k.ToString();
            KeyCode keyNumber = (KeyCode)System.Enum.Parse(typeof(KeyCode), keyString);

            if (Input.GetKeyUp(keyNumber) && previousPlayerPos == currentPlayerPos)
            {
                // swtich case idx 
                messageSwitchCase.values.Add(k);
                osc.Send(messageSwitchCase);

               
            }
        }

        #endregion
    }
}
