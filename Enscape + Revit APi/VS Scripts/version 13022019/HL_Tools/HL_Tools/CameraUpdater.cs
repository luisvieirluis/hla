﻿#region Namespaces
using Autodesk.Revit.DB;

using System;
using System.Collections.Generic;
using System.Diagnostics;
#endregion

namespace HL_Tools
{
    /// <summary>
    /// this is script is related with the update callback to 
    /// retrieve data from the Revit project in real-time 
    /// 
    /// In this case, the data extracted is the camera element and its coordenates
    /// </summary>
    public class CameraUpdater : IUpdater
    {
        public bool cameraMoved;
        public float cameraX;
        public float cameraY;
        public float cameraZ;

        static AddInId m_appId;
        static UpdaterId m_updaterId;


        public int[][,] gridArray;
        public string playerPositionInGrid;

        public Command gridData = new Command();


        public double minNormalisedLim = 0;
        public double maxNormalisedLim = 1.41f;
        public double[][] normalizedGain;

        // constructor takes the AddInId for the add-in associated with this updater
        public CameraUpdater(AddInId id)
        {
            m_appId = id;
            m_updaterId = new UpdaterId(m_appId, new Guid("a717c042-a60b-4f1d-80d4-cda48813faec"));
        }

        public void Execute(UpdaterData data)
        {
            Document doc = data.GetDocument();

            #region FILTER PROJECT TO FETCH CAMERA ELEMENT
            // call sampleCollector function
            ElementCollector sc = new ElementCollector();

            // calling collector for enscape camera
            List<FamilyInstance> GetEnscapeCamera_Class = sc.GetEnscapeCamera_Class(doc);
            Element Camera_ByNameLINQ = sc.GetCameraByNameLINQ(doc, "EnscapeCamera");
            Element CameraElement = sc.GetCameraByNameLambda(doc, "EnscapeCamera");

            //if(CameraElement != null)
            //{
            //    Debug.WriteLine("the camera Element found was " + CameraElement.Name);
            //}

            FamilyInstance cameraFamily = CameraElement as FamilyInstance;
            #endregion

            #region CAMERA POSITION

            // Call cameraposition function
            CameraPosition cp = new CameraPosition();

            // calling XYZ positional data for enscape camera, using Camera_ByNameLambda 
            // variable from filtering method "lambda"
            XYZ cameraPositionData = cp.LocationDataXYZ(CameraElement);

            cameraX = (float)(cameraPositionData.X / 3.28f); // 3.28f to convert to meters
            cameraY = (float)(cameraPositionData.Y / 3.28f);
            cameraZ = (float)(cameraPositionData.Z / 3.28f);
            #endregion

            #region CAMERA ORIENTATION
            // Call cameraOrientatioon function
            CameraOrientation co = new CameraOrientation();

            // calling orientation data for enscape camera
            double cameraAngle = co.GetRotation(cp.LocationDataDouble(CameraElement)); // azimuth rotation
            #endregion


            #region PLAYER INTERPOLATION

            //  not the way to do it (hardcoded)
            // idealy get the grid array from pressing the Henning Larsen button in Revit (command.cs)
            gridArray = new int[10][,]
            {
                new int[,] { { 0,0 }, { 1, 0 } ,{ 2,0 } ,{ 3, 0} ,{ 4,0 }, { 5, 0 }, { 6,0 } ,{7,0 }, { 8, 0 }, { 9, 0 } },
                new int[,] { { 0,1 }, { 1, 1 } ,{ 2,1 } ,{ 3,1 } ,{ 4,1 }, { 5, 1 }, { 6,1 } ,{7,1 }, { 8, 1 }, { 9, 1 } },
                new int[,] { { 0,2 }, { 1, 2 } ,{ 2,2 } ,{ 3,2 } ,{ 4,2 }, { 5, 2 }, { 6,2 } ,{7,2 }, { 8, 2 }, { 9, 2 } },
                new int[,] { { 0,3 }, { 1, 3 } ,{ 2,3 } ,{ 3,3 } ,{ 4,3 }, { 5, 3 }, { 6,3 } ,{7,3 }, { 8, 3 }, { 9, 3 } },
                new int[,] { { 0,4 }, { 1, 4 } ,{ 2,4 } ,{ 3,4 } ,{ 4,4 }, { 5, 4 }, { 6,4 } ,{7,4 }, { 8, 4 }, { 9, 4 } },
                new int[,] { { 0,5 }, { 1, 5 } ,{ 2,5 } ,{ 3,5 } ,{ 4,5 } },
                new int[,] { { 0,6 }, { 1, 6 } ,{ 2,6 } ,{ 3,6 } ,{ 4,6 } },
                new int[,] { { 0,7 }, { 1, 7 } ,{ 2,7 } ,{ 3,7 } ,{ 4,7 } },
                new int[,] { { 0,8 }, { 1, 8 } ,{ 2,8 } ,{ 3,8 } ,{ 4,8 } },
                new int[,] { { 0,9 }, { 1, 9 } ,{ 2,9 } ,{ 3,9 } ,{ 4,9 } }
            };

            if (cameraX >= 0 && cameraY >= 0)
            {
                int playerCameraX = (int)cameraX;
                int playerCameraY = (int)cameraY;

                //playerPositionInGrid = string.Join(",", playerCameraX, playerCameraY);

                if (playerCameraX == gridArray[playerCameraY][playerCameraX, 0] && playerCameraY == gridArray[playerCameraY][playerCameraX, 1])
                {
                    // there has to be 4 points of possible interpolation arround the player position 
                    // if player is inside the building, the possible coordinates are calculated as 
                    // playerCameraX and playerCameraX + 1
                    // playerCameraY and playerCameraY + 1

                    int[][,] interpolationSquare = new int[4][,]
                    {
                        new int[,] { { playerCameraX, playerCameraY } },
                        new int[,] { { playerCameraX, (playerCameraY + 1) } },
                        new int[,] { { (playerCameraX + 1), playerCameraY } },
                        new int[,] { { (playerCameraX + 1), (playerCameraY + 1) } }
                    };


                    // calculate euclidean distance between the four possible points (rounded to 2 decimal houses)

                    double[][] euclideanDistSquare = new double[4][]
                    {
                        new double[] { Math.Round(Math.Sqrt(Math.Pow((cameraX - playerCameraX), 2) + Math.Pow((cameraY - playerCameraY), 2)), 2) },
                        new double[] { Math.Round(Math.Sqrt(Math.Pow((cameraX - playerCameraX), 2) + Math.Pow((cameraY - (playerCameraY + 1)), 2)), 2) },
                        new double[] { Math.Round(Math.Sqrt(Math.Pow((cameraX - (playerCameraX + 1)), 2) + Math.Pow((cameraY - playerCameraY), 2)), 2) },
                        new double[] { Math.Round(Math.Sqrt(Math.Pow((cameraX - (playerCameraX + 1)), 2) + Math.Pow((cameraY - (playerCameraY + 1)), 2)), 2) }
                    };

                    // translate euclidean distances to gain weight for each point
                    // linear normalize values between 0 - 1

                    normalizedGain = new double[4][]
                    {
                        new double[] { Math.Round(((euclideanDistSquare[0][0] - minNormalisedLim) / (maxNormalisedLim - minNormalisedLim)),2) },
                        new double[] { Math.Round(((euclideanDistSquare[1][0] - minNormalisedLim) / (maxNormalisedLim - minNormalisedLim)),2) },
                        new double[] { Math.Round(((euclideanDistSquare[2][0] - minNormalisedLim) / (maxNormalisedLim - minNormalisedLim)),2) },
                        new double[] { Math.Round(((euclideanDistSquare[3][0] - minNormalisedLim) / (maxNormalisedLim - minNormalisedLim)),2) }
                    };

                    //Debug.WriteLine("p1 " + euclideanDistSquare[0][0] + " p1 norm " + normalizedGain[0][0]);
                    //Debug.WriteLine("p2 " + euclideanDistSquare[1][0] + " p2 norm " + normalizedGain[1][0]);
                    //Debug.WriteLine("p3 " + euclideanDistSquare[2][0] + " p3 norm " + normalizedGain[2][0]);
                    //Debug.WriteLine("p4 " + euclideanDistSquare[3][0] + " p4 norm " + normalizedGain[3][0]);




                    #region TO ADAPT TO PURE DATA PATCH
                    // for the values that belong to the interpolation square, send gain values,
                    // all others, send zero gain

                    string[][] gridNumbering = new string[11][]
                    {
                        new string[] { "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve", "diez","null" },
                        new string[] { "duno", "ddos",  "dtres",  "dcuatro" , "dcinco", "dseis", "dsiete", "docho", "dnueve", "ddiez","null" },
                        new string[] { "vuno", "vdos", "vtres",  "vcuatro" , "vcinco", "vseis","vsiete", "vocho", "vnueve", "vdiez","null" },
                        new string[] { "tuno", "tdos", "ttres", "tcuatro", "tcinco", "tseis", "tsiete", "tocho", "tnueve", "tdiez","null"  },
                        new string[] { "cuno", "cdos", "ctres", "ccuatro", "ccinco", "cseis", "csiete", "cocho", "cnueve", "cdiez" ,"null"},
                        new string[] { "ciuno", "cidos", "citres", "cicuatro", "cicinco", "null", "null" , "null" , "null" , "null" , "null"  },
                        new string[] { "ciseis", "cisiete", "ciocho", "cinueve","cidiez", "null" },
                        new string[] { "suno", "sdos", "stres", "scuatro", "scinco", "null" } ,
                        new string[] { "sseis", "ssiete", "socho", "snueve", "sdiez" ,"null"},
                        new string[] { "funo", "fdos", "ftres","fcuatro", "fcinco","null" },
                        new string[] { "null", "null", "null", "null", "null", "null" }
                    };



                    string interpolationPoint1 = gridNumbering[interpolationSquare[0][0, 1]][interpolationSquare[0][0, 0]];
                    string interpolationPoint2 = gridNumbering[interpolationSquare[1][0, 1]][interpolationSquare[1][0, 0]];
                    string interpolationPoint3 = gridNumbering[interpolationSquare[2][0, 1]][interpolationSquare[2][0, 0]];
                    string interpolationPoint4 = gridNumbering[interpolationSquare[3][0, 1]][interpolationSquare[3][0, 0]];

                    string[] gridNullGains = new string[75];


                    double previousCameraX = 0;
                    double previousCameraY = 0;



                    if (previousCameraX != cameraX || previousCameraY != cameraY)
                    {
                        int i = 0;
                        previousCameraX = cameraX;
                        previousCameraY = cameraY;

                      
                        // to all the other indeces, attribute a gain value of "0"
                        foreach (string[] gridRow in gridNumbering)
                        {
                            foreach (string gridPoint in gridRow)
                            {
                                if (gridPoint == interpolationPoint1 || gridPoint == interpolationPoint2 || gridPoint == interpolationPoint3 || gridPoint == interpolationPoint4)
                                {
                                    continue;
                                }
                                else if (gridPoint == "null")
                                {
                                    continue;
                                }
                                else
                                {
                                    // grid point with 0 value for gain
                                    gridNullGains[i] = gridPoint + " " + "0";
                                }
                                i++;
                            }
                        }

                        #region OSC communication

                        // format messages to OSC

                        string messageStringP0 = gridNumbering[interpolationSquare[0][0, 1]][interpolationSquare[0][0, 0]] + " " + normalizedGain[0][0].ToString();
                        string messageStringP1 = gridNumbering[interpolationSquare[1][0, 1]][interpolationSquare[1][0, 0]] + " " + normalizedGain[1][0].ToString();
                        string messageStringP2 = gridNumbering[interpolationSquare[2][0, 1]][interpolationSquare[2][0, 0]] + " " + normalizedGain[2][0].ToString();
                        string messageStringP3 = gridNumbering[interpolationSquare[3][0, 1]][interpolationSquare[3][0, 0]] + " " + normalizedGain[3][0].ToString();

                        var messageString = messageStringP0 + "; " + messageStringP1 + "; " +messageStringP2 + "; " + messageStringP3;

                        Debug.WriteLine(messageString);

                        var message = new SharpOSC.OscMessage(messageString, 1);
                        var sender = new SharpOSC.UDPSender("127.0.0.1", 55555);
                        sender.Send(message);
                        
                        #endregion

                    }

                    #endregion
                }
            }
            else
            {
                Debug.WriteLine("player is not in the grid");
            }

            #endregion
        }

        public string GetAdditionalInformation()
        {
            return "Wall type updater example: updates all newly created walls to a special wall";
        }

        public ChangePriority GetChangePriority()
        {
            return ChangePriority.FloorsRoofsStructuralWalls;
        }

        public UpdaterId GetUpdaterId()
        {
            return m_updaterId;
        }

        public string GetUpdaterName()
        {
            return "System Updater";
        }


    

    }
}