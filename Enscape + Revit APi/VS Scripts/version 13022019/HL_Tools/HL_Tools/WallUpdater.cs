﻿#region Namespaces
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;
#endregion

namespace HL_Tools
{

    public class WallUpdater : IUpdater
    {
        public bool mouseClicked;

        static AddInId m_appId;
        static UpdaterId m_updaterId;
        FamilyInstance cameraType = null;

        // constructor takes the AddInId for the add-in associated with this updater
        public WallUpdater(AddInId id)
        {
            m_appId = id;
            m_updaterId = new UpdaterId(m_appId, new Guid("a717c042-a60b-4f1d-80d4-cda48813faec"));
        }

        public void Execute(UpdaterData data)
        {
            Document doc = data.GetDocument();

            SampleCollector sc = new SampleCollector();

            // calling collector for enscape camera
            List<FamilyInstance> GetEnscapeCamera_Class = sc.GetEnscapeCamera_Class(doc);
            Element Camera_ByNameLINQ = sc.GetCameraByNameLINQ(doc, "EnscapeCamera");
            Element Camera_ByNameLambda = sc.GetCameraByNameLambda(doc, "EnscapeCamera");

            cameraType = Camera_ByNameLambda as FamilyInstance;




        
        }

        public string GetAdditionalInformation()
        {
            return "Wall type updater example: updates all newly created walls to a special wall";
        }

        public ChangePriority GetChangePriority()
        {
            return ChangePriority.FloorsRoofsStructuralWalls;
        }

        public UpdaterId GetUpdaterId()
        {
            return m_updaterId;
        }

        public string GetUpdaterName()
        {
            return "Wall Type Updater";
        }

        // check left mouse button state
        public void mouseMoveEventHandler(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                //do left stuff
                mouseClicked = true;
            }
            else
            {
                mouseClicked = false;
                // do other stuff
            }
        }



    }

}