﻿using Autodesk.Revit.DB;
using System;

namespace HL_Tools
{
    class CameraOrientation
    {
        // ================================================================================================ //
        // ======================= handling Enscape Camera Orientational Data ============================= //
        // ================================================================================================ //



        //public XYZ GetRotation(FamilyInstance elementFamily)
        //{

        //    XYZ CameraOrient = elementFamily.FacingOrientation as XYZ;
        //    CameraOrient = CameraOrient * (180.0f / Math.PI);

        //    return CameraOrient;
        //}

        public double GetRotation(LocationPoint cameraLocation)
        {
            // (180.0f / Math.PI) used to convert to degrees
            double cameraRotation = cameraLocation.Rotation * (180.0f / Math.PI);

            return cameraRotation;
        }
    }
}
