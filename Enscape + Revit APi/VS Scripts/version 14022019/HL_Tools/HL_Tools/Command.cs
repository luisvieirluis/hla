#region Dependencies
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;


#endregion

namespace HL_Tools
{
    [Transaction(TransactionMode.Manual)]
    public class Command : IExternalCommand
    {
        // initialize the grid array by defining the size

        //public string[,] gridCoordinateArray = new string[56, 56];
                    public int[][,] gridCalc;
        public int[][,] gridCoordinateArray;
        public List<string> gridCoordinateList;
        public int xLim = 4;

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            // Application and Document 
            UIApplication uiapp = commandData.Application;
            UIDocument uidoc = uiapp.ActiveUIDocument;
            Autodesk.Revit.ApplicationServices.Application app = uiapp.Application;
            Document doc = uidoc.Document;

            // ======================================================================================= //
            #region Grid Calculation

            // --------------------------------------------------------------------------------------- //
            // 1. calculate the size of a square grid by defining the distance between points

            // user defined value (for now, hard coded)
            double interpolationDistance = 1.0f;

            // value fetch from selecting the longest wall
            double lengthOfWall = 10.0f;

            // grid size contains the number of points that will define the interpolation grid
            double lengthOfGrid = lengthOfWall - interpolationDistance;

            // --------------------------------------------------------------------------------------- //
            // 2. calculate the coordinates for each point in the grid

            XYZ wallInfo = GetWallInfo(uidoc, doc);

            // gridCoordinateArray creation
            int[][,] gridCalc = GridCalculation(lengthOfGrid, interpolationDistance, wallInfo);


            #endregion

            return Result.Succeeded;
        }

        #region WALL END-POINTS COORDINATES
        public XYZ GetWallInfo(UIDocument uidoc, Document doc)
        {
            Reference wallReference = uidoc.Selection.PickObject(ObjectType.Element);
            Element wallElement = doc.GetElement(wallReference);

            //ElementCollector sc = new ElementCollector();
            //// calling collector for enscape camera
            //Element wallElement = sc.GetCameraByNameLambda(doc, "Wall 1");

            // get the current wall location
            LocationCurve wallLocation = wallElement.Location as LocationCurve;

            // get the points
            XYZ wallPt1 = wallLocation.Curve.GetEndPoint(0);
            XYZ wallPt2 = wallLocation.Curve.GetEndPoint(1);

            Parameter wallLengthElement = wallElement.get_Parameter(BuiltInParameter.CURVE_ELEM_LENGTH);

            double feet2Meter = 03048f; // convert fee to meter ratio
            double lengthOfWall = wallLengthElement.AsDouble() * feet2Meter; // wall length in meters
            // Debug.WriteLine("Wall Length is " + lengthOfWall);

            return wallPt1;
        }
        #endregion

        #region INTERPOLATION GRID CALCULATION
        public int[][,] GridCalculation(double lengthOfGrid, double interpolationDistance, XYZ wallReference)
        {



            //double gridSize = Math.Pow(interpolationDistance, 2);

            // initialize 2D string array to create an array with the coordinates for each point in the grid
            string[,] coordinatePointArray = new string[(int)lengthOfGrid, (int)lengthOfGrid];

            // for each row value, there has to be all the collum values
            // the loop creates a string value for each of these coordinate points
            for (int rowIdx = 0; rowIdx < lengthOfGrid; rowIdx++)
            {
                double coordinateX = Math.Round((wallReference.X + (interpolationDistance + rowIdx)) * 100) / 100;
                string coordinateXString = coordinateX.ToString();

                for (int colIdx = 0; colIdx < lengthOfGrid; colIdx++)
                {
                    double coordinateY = Math.Round((wallReference.Y + (interpolationDistance + colIdx)) * 100) / 100;
                    string coordinateYString = coordinateY.ToString();

                    coordinatePointArray[rowIdx, colIdx] = String.Format("{0};{1}", coordinateX, coordinateY);
                }
            }


            gridCoordinateList = new List<string>();

            // Hardcoded boundaries
            for (int gridCol = 0; gridCol < lengthOfGrid; gridCol++)
            {
                Debug.WriteLine(gridCol);

                if (gridCol <= xLim)
                {
                    for (int gridRow = 0; gridRow < lengthOfGrid; gridRow++)
                    {
                        gridCoordinateList.Add(coordinatePointArray[gridRow, gridCol]);
                        //Debug.WriteLine("grid point is now: " + String.Join(",", gridCoordinateList.ToArray()));
                    }
                }
                else if (gridCol > xLim)
                {
                    for (int gridRow = 0; gridRow < xLim; gridRow++)
                    {
                        gridCoordinateList.Add(coordinatePointArray[gridRow, gridCol]);
                        //Debug.WriteLine("grid point is now: " + String.Join(",", gridCoordinateList.ToArray()));

                        //gridCoordinateArray[gridRow, gridCol] = coordinatePointArray[gridRow, gridCol];
                        // Debug.WriteLine("grid point is now: " + gridCoordinateArray[gridRow, gridCol]);
                    }
                }
            }

            return gridCoordinateArray;
        }
        #endregion



        // OPTION 2 : USE ROOM GEOMETRY TO FETCH THE POSITIONAL DATA OF THE WALLS
        #region ROOM INFO
        //// having the room coordinates, it is possible to find which grid points are inside the boundaries of the room 
        //public void GetRoomInfo(UIDocument uidoc, Document doc)
        //{
        //    //Reference roomReference = uidoc.Selection.PickObject(ObjectType.Element);
        //    //Room roomElement = doc.GetElement(roomReference) as Room;
        //    //Element RoomPoint = doc.GetElement(roomReference);

        //    ElementCollector sc = new ElementCollector();
        //    // calling collector for enscape camera
        //    Element roomFilter = sc.GetCameraByNameLambda(doc, "Room1");
        //    Room roomElement = roomFilter as Room;

        //    Debug.WriteLine("room element was found and it is called " + roomFilter.Name);


        //    ////get the room position
        //    //LocationPoint roomLocation = RoomPoint.Location as LocationPoint;
        //    //XYZ roomPoint = roomLocation.Point;
        //    //Debug.WriteLine("Room Coordinates" + roomPoint.X + "," + roomPoint.Y + "," + roomPoint.Z);

        //    SpatialElementBoundaryOptions options = new SpatialElementBoundaryOptions();
        //    options.SpatialElementBoundaryLocation = SpatialElementBoundaryLocation.Finish;
        //    string roomElementInfo = "";

        //    foreach (IList<BoundarySegment> boundSegList in roomElement.GetBoundarySegments(options))
        //    {
        //        foreach (BoundarySegment boundSeg in boundSegList)
        //        {
        //            ElementId eID = boundSeg.ElementId;
        //            Element e = doc.GetElement(eID);
        //            Wall wall = e as Wall;
        //            LocationCurve locationCurve = wall.Location as LocationCurve;
        //            Curve curve = locationCurve.Curve;

        //            // length parameter
        //            Parameter wallParameter = wall.get_Parameter(BuiltInParameter.CURVE_ELEM_LENGTH);
        //            double feet2Meter = 03048f; // convert fee to meter ratio
        //            double lengthOfWall = wallParameter.AsDouble() * feet2Meter; // wall length in meters

        //            roomElementInfo += e.Name + " " + curve.Length + "\n";
        //        }
        //    }

        //    TaskDialog.Show("Boundary Segment Elements", roomElementInfo);

        //    //return roomPoint;
        //}
        #endregion

        #region INTERPOLATION GRID CALCULATION
        //public string[][,] GridCalculation(double lengthOfGrid, double interpolationDistance)
        //{
        //    double gridSize = Math.Pow(interpolationDistance, 2);
        //    // initialize 2D string array to create an array with the coordinates for each point in the grid
        //    string[,] coordinatePoint = new string[(int)lengthOfGrid, (int)lengthOfGrid];

        //    // for each row value, there has to be all the collum values
        //    // the loop creates a string value for each of these coordinate points
        //    for (int rowIdx = 0; rowIdx < lengthOfGrid; rowIdx++)
        //    {
        //        double coordinateX = (int)(interpolationDistance + rowIdx);
        //        string coordinateXString = coordinateX.ToString();

        //        for (int colIdx = 0; colIdx < lengthOfGrid; colIdx++)
        //        {
        //            double coordinateY = (int)(interpolationDistance + colIdx);
        //            string coordinateYString = coordinateY.ToString();
        //            coordinatePoint[rowIdx, colIdx] = String.Format("{0},{1}", coordinateX, coordinateY);
        //        }
        //    }










        //    // create jagged array (array of arrays) to store all the coordinate points
        //    // that are inside the building boundaries, if multiple floors are considered,
        //    // tag the floor number in the coordinatePoint string message.

        //    gridCoordinateArray = new string[][,]
        //    {


        //    };



        //    return gridCoordinateArray;
        //}
        #endregion




        // ======================================================================================= //
        #region CONVERT ELEMENT ID TO STRING (W/ STRINGBUILDER)
        public StringBuilder SB_Camera(List<FamilyInstance> EnscapeCamera)
        {
            StringBuilder sb = new StringBuilder();
            foreach (FamilyInstance camera in EnscapeCamera)
                sb.Append(camera.Name + " " + camera.Id + "\n");

            return sb;
        }
        #endregion

        #region TEXT FILE OUTPUT FORMAT
        void CreateText(string content)
        {
            //string path = Application.dataPath + "/Log.txt";
            string path = Directory.GetCurrentDirectory();
            string output = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            if (!File.Exists(path))
            {
                File.WriteAllText(path, "Login log \n\n");
            }

            string initContent = "Login date: " + DateTime.Now + "\n"
                + "Interpolation Grid Coordinates ";

            File.AppendAllText(path, content);

        }
        #endregion

    }
}
