﻿#region
using Autodesk.Revit.DB;
using System.Collections.Generic;
using System.Linq;
#endregion

/// <summary>
/// Script to find Enscape camera. 
/// To access generic models in Revit API, refer to FamilyInstance and find element requested (generic model family)
/// using filtered element collector function, by looking for "EnscapeCamera" tag
/// </summary>
namespace HL_Tools
{
    class ElementCollector
    {
        // ======================================================================================= //
        // ======================= handling Enscape Camera Id ==================================== //
        // ======================================================================================= //


        // ================================= GetEnscapeCamera_Class ============================== //
        // this method does not find only the EnscapeCamera but all the elements stored 
        // as part of the Generic Models Family
        public List<FamilyInstance> GetEnscapeCamera_Class(Document doc)
        {
            FilteredElementCollector collector = new FilteredElementCollector(doc);
            ICollection<Element> EnscapeCamera = collector.OfClass(typeof(FamilyInstance)).ToElements();

            List<FamilyInstance> List_EnscapeCamera = new List<FamilyInstance>();
            foreach (FamilyInstance camera in EnscapeCamera)
            {
                List_EnscapeCamera.Add(camera);
            }
            return List_EnscapeCamera;
        }

        // ================================= GetCameraByNameLINQ ================================= //
        // this method does finds the EnscapeCamera by refering to the precise name of the element
        // in theory it's a slower process than the class finder.
        public FamilyInstance GetCameraByNameLINQ(Document doc, string name)
        {
            FamilyInstance camera = (from v in new FilteredElementCollector(doc)
                                     .OfClass(typeof(FamilyInstance))
                                     .Cast<FamilyInstance>()
                         where v.Name == name
                         select v).First();
            return camera;
        }

        // ================================= GetCameraByNameLambda================================= //
        // this method finds the EnscapeCamera by using lambda reference to match the input name
        public Element GetCameraByNameLambda(Document doc, string name)
        {
            return new FilteredElementCollector(doc)
              .OfClass(typeof(FamilyInstance))
              .FirstOrDefault<Element>(
                e => e.Name.Equals(name));
        }
    }
}
