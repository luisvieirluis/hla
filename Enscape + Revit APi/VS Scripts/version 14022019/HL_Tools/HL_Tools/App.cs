#region Namespaces
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows.Media.Imaging;
#endregion

namespace HL_Tools
{
    class App : IExternalApplication
    {

        const string tabName = "Henning Larsen ToolKit";
        const string panelName = "VR Acoustics";

        // ======================================================================================= //
        public Result OnStartup(UIControlledApplication a)
        {
           

            #region RIBBON BUTTON
            // generate a panel and tab through the fuction bellow
            RibbonPanel panel = RibbonPanel(a);

            // look a the assembly where the plugin is built
            string thisAssemblyPath = Assembly.GetExecutingAssembly().Location;

            // generate button
            PushButton button = panel.AddItem(new PushButtonData("HL Plugin", panelName, thisAssemblyPath, "HL_Tools.Command")) as PushButton;


            button.ToolTip = "this is a sample";
            //// Reflection of path to image 
            //var globePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "Bitmap16.bmp");
            //Uri uriImage = new Uri(globePath);
            //// Apply image to bitmap
            //BitmapImage largeImage = new BitmapImage(uriImage);
            //// Apply image to button 
            //button.LargeImage = largeImage; 
            #endregion

            #region IDLING AND CLOSIING
            a.ApplicationClosing += A_ApplicationClosing;

            // set application to idling
            a.Idling += A_Idling;

            #endregion

            #region UPDATER 
            // Register wall updater with Revit
            CameraUpdater updater = new CameraUpdater(a.ActiveAddInId);
            UpdaterRegistry.RegisterUpdater(updater);

            // Set the filter (Enscape camera)
            ElementClassFilter cameraFilter = new ElementClassFilter(typeof(FamilyInstance));
            // Add trigger
            UpdaterRegistry.AddTrigger(updater.GetUpdaterId(), cameraFilter, Element.GetChangeTypeGeometry());

            #endregion

            return Result.Succeeded;
        }

        // ======================================================================================= //
        public Result OnShutdown(UIControlledApplication a)
        {
            CameraUpdater updater = new CameraUpdater(a.ActiveAddInId);
            UpdaterRegistry.UnregisterUpdater(updater.GetUpdaterId());
            

            return Result.Succeeded;
        }
        
        #region IDLING & CLOSING APP

        void A_Idling(object sender, Autodesk.Revit.UI.Events.IdlingEventArgs e)
        {

        }

        // ======================================================================================= //
        void A_ApplicationClosing(object sender, Autodesk.Revit.UI.Events.ApplicationClosingEventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region RIBBON PANEL

        public RibbonPanel RibbonPanel(UIControlledApplication a)
        {
            // Empty ribbon panel 
            RibbonPanel ribbonPanel = null;

            // try to create a ribbon tab
            try
            {
                a.CreateRibbonTab(tabName);
            }
            catch { }

            // Try to create ribbonn panel
            try
            {
                RibbonPanel panel = a.CreateRibbonPanel(tabName, panelName);
            }
            catch { }

            // check if tabName exists in the panels created
            List<RibbonPanel> panels = a.GetRibbonPanels(tabName);
            foreach (RibbonPanel p in panels)
            {
                if (p.Name == panelName)
                {
                    ribbonPanel = p;
                }
            }

            return ribbonPanel;
        }

        #endregion


    }
}
