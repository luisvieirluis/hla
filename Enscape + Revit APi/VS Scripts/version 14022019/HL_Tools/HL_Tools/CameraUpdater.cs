﻿#region Namespaces
using Autodesk.Revit.DB;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
#endregion

namespace HL_Tools
{
    /// <summary>
    /// this is script is related with the update callback to 
    /// retrieve data from the Revit project in real-time 
    /// 
    /// In this case, the data extracted is the camera element and its coordenates
    /// </summary>
    public class CameraUpdater : IUpdater
    {
        public bool cameraMoved;
        public float cameraX;
        public float cameraY;
        public float cameraZ;

        static AddInId m_appId;
        static UpdaterId m_updaterId;


        public int[][,] gridArray;
        public string playerPositionInGrid;

        public Command gridData = new Command();

        // ----------------------------------------------------------------------------------- //

        public double minNormalisedLim = 0;
        public double maxNormalisedLim = 1.41f;
        public double[] normalisedEuclidDist;

        // ----------------------------------------------------------------------------------- //

        int toggledKey = 0;

        int port = 8888;

        // constructor takes the AddInId for the add-in associated with this updater
        public CameraUpdater(AddInId id)
        {
            m_appId = id;
            m_updaterId = new UpdaterId(m_appId, new Guid("a717c042-a60b-4f1d-80d4-cda48813faec"));
        }

        public void Execute(UpdaterData data)
        {
            Document doc = data.GetDocument();

            #region FILTER PROJECT TO FETCH CAMERA ELEMENT
            // call sampleCollector function
            ElementCollector sc = new ElementCollector();

            // calling collector for enscape camera
            List<FamilyInstance> GetEnscapeCamera_Class = sc.GetEnscapeCamera_Class(doc);
            Element Camera_ByNameLINQ = sc.GetCameraByNameLINQ(doc, "EnscapeCamera");
            Element CameraElement = sc.GetCameraByNameLambda(doc, "EnscapeCamera");


            FamilyInstance cameraFamily = CameraElement as FamilyInstance;
            #endregion

            #region CAMERA POSITION

            // Call cameraposition function
            CameraPosition cp = new CameraPosition();

            // calling XYZ positional data for enscape camera, using Camera_ByNameLambda 
            // variable from filtering method "lambda"
            XYZ cameraPositionData = cp.LocationDataXYZ(CameraElement);

            cameraX = (float)(cameraPositionData.X / 3.28f); // 3.28f to convert to meters
            cameraY = (float)(cameraPositionData.Y / 3.28f);
            cameraZ = (float)(cameraPositionData.Z / 3.28f);
            #endregion

            #region CAMERA ORIENTATION
            // Call cameraOrientatioon function
            CameraOrientation co = new CameraOrientation();

            // calling orientation data for enscape camera
            double cameraAngle = co.GetRotation(cp.LocationDataDouble(CameraElement)); // azimuth rotation
            double cameraEle = co.GetElevation(CameraElement);
            #endregion

            #region PLAYER INTERPOLATION

            //  not the way to do it (hardcoded)
            // idealy get the grid array from pressing the Henning Larsen button in Revit (command.cs)
            gridArray = new int[10][,]
            {
                new int[,] { { 0,0 }, { 1, 0 } ,{ 2,0 } ,{ 3, 0} ,{ 4,0 }, { 5, 0 }, { 6,0 } ,{7,0 }, { 8, 0 }, { 9, 0 } },
                new int[,] { { 0,1 }, { 1, 1 } ,{ 2,1 } ,{ 3,1 } ,{ 4,1 }, { 5, 1 }, { 6,1 } ,{7,1 }, { 8, 1 }, { 9, 1 } },
                new int[,] { { 0,2 }, { 1, 2 } ,{ 2,2 } ,{ 3,2 } ,{ 4,2 }, { 5, 2 }, { 6,2 } ,{7,2 }, { 8, 2 }, { 9, 2 } },
                new int[,] { { 0,3 }, { 1, 3 } ,{ 2,3 } ,{ 3,3 } ,{ 4,3 }, { 5, 3 }, { 6,3 } ,{7,3 }, { 8, 3 }, { 9, 3 } },
                new int[,] { { 0,4 }, { 1, 4 } ,{ 2,4 } ,{ 3,4 } ,{ 4,4 }, { 5, 4 }, { 6,4 } ,{7,4 }, { 8, 4 }, { 9, 4 } },
                new int[,] { { 0,5 }, { 1, 5 } ,{ 2,5 } ,{ 3,5 } ,{ 4,5 } },
                new int[,] { { 0,6 }, { 1, 6 } ,{ 2,6 } ,{ 3,6 } ,{ 4,6 } },
                new int[,] { { 0,7 }, { 1, 7 } ,{ 2,7 } ,{ 3,7 } ,{ 4,7 } },
                new int[,] { { 0,8 }, { 1, 8 } ,{ 2,8 } ,{ 3,8 } ,{ 4,8 } },
                new int[,] { { 0,9 }, { 1, 9 } ,{ 2,9 } ,{ 3,9 } ,{ 4,9 } }
            };

            if (cameraX >= 0 && cameraY >= 0)
            {
                int playerCameraX = (int)cameraX;
                int playerCameraY = (int)cameraY;

                //playerPositionInGrid = string.Join(",", playerCameraX, playerCameraY);

                if (playerCameraX == gridArray[playerCameraY][playerCameraX, 0] && playerCameraY == gridArray[playerCameraY][playerCameraX, 1])
                {

                    #region CALCULATE INTERPOLATION POINTS AND NORMALISE THE EUCLIDEAN DIST TO 0-1 RANGE
                    // there has to be 4 points of possible interpolation arround the player position 
                    // if player is inside the building, the possible coordinates are calculated as 
                    // playerCameraX and playerCameraX + 1
                    // playerCameraY and playerCameraY + 1

                    int[][,] interpolationSquare = new int[4][,]
                    {
                        new int[,] { { playerCameraX, playerCameraY } },
                        new int[,] { { playerCameraX, (playerCameraY + 1) } },
                        new int[,] { { (playerCameraX + 1), playerCameraY } },
                        new int[,] { { (playerCameraX + 1), (playerCameraY + 1) } }
                    };


                    // calculate euclidean distance between the four possible points (rounded to 2 decimal houses)

                    double[][] euclideanDistSquare = new double[4][]
                    {
                        new double[] { Math.Round(Math.Sqrt(Math.Pow((cameraX - playerCameraX), 2) + Math.Pow((cameraY - playerCameraY), 2)), 2) },
                        new double[] { Math.Round(Math.Sqrt(Math.Pow((cameraX - playerCameraX), 2) + Math.Pow((cameraY - (playerCameraY + 1)), 2)), 2) },
                        new double[] { Math.Round(Math.Sqrt(Math.Pow((cameraX - (playerCameraX + 1)), 2) + Math.Pow((cameraY - playerCameraY), 2)), 2) },
                        new double[] { Math.Round(Math.Sqrt(Math.Pow((cameraX - (playerCameraX + 1)), 2) + Math.Pow((cameraY - (playerCameraY + 1)), 2)), 2) }
                    };

                    
                    // translate euclidean distances to gain weight for each point
                    // linear normalize values between 0 - 1


                    normalisedEuclidDist = new double[4]
                    {
                        Math.Round(((euclideanDistSquare[0][0] - minNormalisedLim) / (maxNormalisedLim - minNormalisedLim)),2) ,
                        Math.Round(((euclideanDistSquare[1][0] - minNormalisedLim) / (maxNormalisedLim - minNormalisedLim)),2) ,
                        Math.Round(((euclideanDistSquare[2][0] - minNormalisedLim) / (maxNormalisedLim - minNormalisedLim)),2) ,
                        Math.Round(((euclideanDistSquare[3][0] - minNormalisedLim) / (maxNormalisedLim - minNormalisedLim)),2)
                    };

                    #endregion


                    #region TO ADAPT TO PURE DATA PATCH


                    #region TOGGLE MATERIAL

                    if (Keyboard.IsKeyDown(Key.P))
                    {
                        toggledKey = 1;
                        //Debug.WriteLine("key is toggled and value is " + toggledKey);

                        //-------------------------------------------------------------------------------//

                        string toggleValue = "toggle" + " " + toggledKey.ToString();
                        var messageTogg = new SharpOSC.OscMessage(toggleValue);
                        var senderTogg = new SharpOSC.UDPSender("127.0.0.1", port);
                        senderTogg.Send(messageTogg);

                    }
                    else if (Keyboard.IsKeyDown(Key.O))
                    {
                        toggledKey = 0;
                        //Debug.WriteLine("key is not toggled and value is " + toggledKey);

                        //-------------------------------------------------------------------------------//

                        string toggleValue = "toggle" + " " + toggledKey.ToString();
                        var messageTogg = new SharpOSC.OscMessage(toggleValue);
                        var senderTogg = new SharpOSC.UDPSender("127.0.0.1", port);
                        senderTogg.Send(messageTogg);
                    }

                    #endregion


                    // for the values that belong to the interpolation square, send gain values,
                    // all others, send zero gain

                    string[][] gridNumbering = new string[11][]
                    {
                        new string[] { "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null" },
                        new string[] { "null", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve","null" },
                        new string[] { "null", "duno", "ddos",  "dtres",  "dcuatro" , "dcinco", "dseis", "dsiete", "docho", "dnueve","null" },
                        new string[] { "null", "vuno", "vdos", "vtres",  "vcuatro" , "vcinco", "vseis","vsiete", "vocho", "vnueve","null" },
                        new string[] { "null", "tuno", "tdos", "ttres", "tcuatro", "tcinco", "tseis", "tsiete", "tocho", "tnueve", "null"  },
                        new string[] { "null", "cuno", "cdos", "ctres", "ccuatro", "null", "null", "null", "null", "null", "null"},
                        new string[] { "null", "ciuno", "cidos", "citres", "cicuatro", "null" },
                        new string[] { "null", "ciseis", "cisiete", "ciocho", "cinueve", "null" },
                        new string[] { "null", "suno", "sdos", "stres", "scuatro", "null" } ,
                        new string[] { "null", "sseis", "ssiete", "socho", "snueve", "null"},
                        new string[] { "null", "null", "null", "null", "null", "null" }
                    };


                    // ============================================================================================================= //

                    // check if any of the interpolation points is a "null" point in the grid 
                    // if so, then remove it from the array and compensate the gain weighting by measuring the length of the new interpolation array
                    // if length is 1, then there is only one available point for interpolation, if the length is 2, there are two points of interpolation

                    // gridNumbering[colIdx][rowIdx]
                    string interpolationPoint1 = gridNumbering[interpolationSquare[0][0, 1]][interpolationSquare[0][0, 0]];
                    string interpolationPoint2 = gridNumbering[interpolationSquare[1][0, 1]][interpolationSquare[1][0, 0]];
                    string interpolationPoint3 = gridNumbering[interpolationSquare[2][0, 1]][interpolationSquare[2][0, 0]];
                    string interpolationPoint4 = gridNumbering[interpolationSquare[3][0, 1]][interpolationSquare[3][0, 0]];

                    string[] interpolationPointArray = new string[] { interpolationPoint1, interpolationPoint2, interpolationPoint3, interpolationPoint4 };

                    #region option A

                    List<string> interpolationNameList = new List<string>();
                    List<double> euclideanArrayList = new List<double>();
                    List<int> indexforEachPoint = new List<int>();

                    for (int i = 0; i < interpolationPointArray.Length; i++)
                    {
                        if (interpolationPointArray[i] == "null")
                        {
                            continue;
                        }
                        else
                        {
                            interpolationNameList.Add(interpolationPointArray[i]);
                            euclideanArrayList.Add(normalisedEuclidDist[i]);
                            indexforEachPoint.Add(i);
                        }
                    }

                    // --------------------------------------------------------------------------------------------------------------- //

                    List<double> gainWeightPoint = new List<double>();
                    double sumArray = euclideanArrayList.Sum();

                    foreach (double elePoint in euclideanArrayList)
                    {
                        double gainElement = ( elePoint / sumArray );

                        gainWeightPoint.Add(gainElement);
                    }

                    // --------------------------------------------------------------------------------------------------------------- //

                    int[] indexArray = indexforEachPoint.ToArray();
                    double[] gainWeight = new double[] { 0f, 0f, 0f, 0f };

                    for (int j = 0; j < interpolationPointArray.Length; j++)
                    {
                        for (int k = 0; k < indexArray.Length; k++)
                        {
                            if (indexforEachPoint[k] == j)
                            {
                                gainWeight[j] = gainWeightPoint[k];
                                break;
                            }
                        }                     
                    }

                    #endregion
                   
                    // ============================================================================================================= //

                    // create an array with all the points in the grid, including the "null" points
                    // go through the array and if the point is a "null", remove it, if the point does not belong to the interpolation array
                    // it should be addressed a value of "0" gain

                    string[] gridNullGains = new string[56];
                    List<string> gridNullGainsList = new List<string>();

                    double previousCameraX = 0;
                    double previousCameraY = 0;
                    
                    if (previousCameraX != cameraX || previousCameraY != cameraY)
                    {
                        int i = 0;
                        previousCameraX = cameraX;
                        previousCameraY = cameraY;


                        // to all the other indeces, attribute a gain value of "0"

                        foreach (string[] gridRow in gridNumbering)
                        {
                            foreach (string gridPoint in gridRow)
                            {
                                if (gridPoint == interpolationPoint1 || gridPoint == interpolationPoint2 || gridPoint == interpolationPoint3 || gridPoint == interpolationPoint4)
                                {
                                    continue;
                                }
                                else if (gridPoint == "null")
                                {
                                    continue;
                                }
                                else
                                {
                                    // grid point with 0 value for gain
                                    gridNullGains[i] = gridPoint + " " + "0";
                                    gridNullGainsList.Add(gridPoint + " " + "0");
                                }
                                i++;
                            }
                        }

                        #region OSC communication

                        // format messages to OSC

                        string Azimuth = "azi" + " " + cameraAngle.ToString().Replace(",", ".");
                        var messsageAzi = new SharpOSC.OscMessage(Azimuth);
                        var senderAzi = new SharpOSC.UDPSender("127.0.0.1", port);
                        senderAzi.Send(messsageAzi);

                        string Elevation = "ele" + " " + cameraEle.ToString().Replace(",", ".");
                        var messsageEle = new SharpOSC.OscMessage(Elevation);
                        var senderEle = new SharpOSC.UDPSender("127.0.0.1", port);
                        senderEle.Send(messsageEle);

                        //-------------------------------------------------------------------------------//

                        string messageStringP0 = interpolationPoint1 + " " + gainWeight[0].ToString().Replace(",", ".");
                        string messageStringP1 = interpolationPoint2 + " " + gainWeight[1].ToString().Replace(",", ".");
                        string messageStringP2 = interpolationPoint3 + " " + gainWeight[2].ToString().Replace(",", ".");
                        string messageStringP3 = interpolationPoint4 + " " + gainWeight[3].ToString().Replace(",", ".");
                        string[] messageStringPoint = new string[4] { messageStringP0, messageStringP1, messageStringP2, messageStringP3 };

                        for (int messageIdx = 0; messageIdx < gridNullGainsList.ToArray().Length - 1; messageIdx++)
                        {
                            string messageString = gridNullGainsList[messageIdx];
                            var messageNulls = new SharpOSC.OscMessage(messageString);
                            var senderNulls = new SharpOSC.UDPSender("127.0.0.1", port);
                            senderNulls.Send(messageNulls);
                        }

                        for (int messageStringIdx = 0; messageStringIdx < 4; messageStringIdx++)
                        {
                            string messageInterpolationString = messageStringPoint[messageStringIdx];
                            var messageInterpolation = new SharpOSC.OscMessage(messageInterpolationString);
                            var sender = new SharpOSC.UDPSender("127.0.0.1", port);
                            sender.Send(messageInterpolation);
                        }


                        #endregion

                    }

                    #endregion
                }
            }
            else
            {
                Debug.WriteLine("player is not in the grid");
            }

            #endregion
        }

        public string GetAdditionalInformation()
        {
            return "Wall type updater example: updates all newly created walls to a special wall";
        }

        public ChangePriority GetChangePriority()
        {
            return ChangePriority.FloorsRoofsStructuralWalls;
        }

        public UpdaterId GetUpdaterId()
        {
            return m_updaterId;
        }

        public string GetUpdaterName()
        {
            return "System Updater";
        }


    }
}