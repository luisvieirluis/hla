﻿using Autodesk.Revit.DB;


namespace HL_Tools
{
    class CameraPosition
    {
        // ================================================================================================ //
        // ======================= handling Enscape Camera Positional Data ================================ //
        // ================================================================================================ //

        // this method gives the coordenates of the selected element as XYZ system
        public XYZ LocationDataXYZ(Element element)
        {
            LocationPoint location = element.Location as LocationPoint;

            return location.Point;
        }

        // this method is used to send location as LocationPoint to find orientation of selected element
        public LocationPoint LocationDataDouble(Element element)
        {
            LocationPoint locationCam = element.Location as LocationPoint;

            return locationCam;
        }
    }
}

